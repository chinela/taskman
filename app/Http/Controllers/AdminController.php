<?php

namespace App\Http\Controllers;

use App\Role;
use App\Department;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function __construct(){
        $this->middleware(['auth', 'isAdmin']);
    }

    public function index(){
        $roles = Role::pluck('name', 'id')->all();

        $departments = Department::orderBy('name', 'asc')->pluck('name', 'id')->all();

        return  view('admin.index', compact('roles', 'departments'));
    }

    
}
