<?php

namespace App\Http\Controllers;

use App\Department;
use App\Task;
use App\User;
use App\Role; 

use Illuminate\Http\Request;

class AdminTasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware(['auth', 'isAdmin']);
    }
    
    public function index()
    {
        $tasks = Task::orderBy('created_at', 'asc')->paginate(10);
        // $tasks = Task::orderBy('created_at', 'asc')->get();
        $x = 1;

        return view('admin.task.view', compact('tasks', 'x'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::orderBy('name', 'asc')->pluck('name', 'id')->all();
        return view('admin.task.create', compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'=> 'required|string',
            'department_id' => 'required'
        ]);

        $task = Task::create(['title'=>$request->title, 'status_id'=>2]);

        $task->departments()->attach($request->department_id);

        return redirect('admin/tasks/create')->with('success', 'New Task Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
