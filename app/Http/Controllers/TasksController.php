<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\TaskStatus;

class TasksController extends Controller
{
    public function completed($id){
        $task = Task::find($id);

        $task->update(['status_id'=>1]);

        return redirect('/home');
    }
}
