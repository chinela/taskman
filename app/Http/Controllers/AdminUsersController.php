<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\EditUserRequest;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Role;
use App\Department;
use App\Photo;

class AdminUsersController extends Controller
{
    
    public function __construct(){
        $this->middleware(['auth', 'isAdmin']);
    }
    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('email', 'asc')->get();

        // foreach($users as $i=>$user){
        //     if(is_null($user->department_id)){
        //         echo "cant't go";
        //     } else {
        //         echo $user->department_id;
        //     }
        // }
        return view('admin.user.view', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->except('image');

        $input['password'] = Hash::make($request->password);

        $newUser = User::create($input);

        if($file = $request->file('image')){
            $name = time() . $file->getClientOriginalName();

            $file->move('images', $name);

            $photo = Photo::create(['image'=>$name]);

            $newUser->photo()->save($photo);
        }

        return redirect('/admin')->with('success', 'One user created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        
        $roles = Role::pluck('name', 'id')->all();

        $departments = Department::orderBy('name', 'asc')->pluck('name', 'id')->all();
        return view('admin.user.edit', compact('user', 'roles', 'departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditUserRequest $request, $id)
    {
        // $input = $request->all();

        $user = User::findOrFail($id);

        if(trim($request->password) === ''){
            $input = $request->except('password');
        } else {
            $input = $request->all();
            $input['password'] = Hash::make($request->password);
        }

        if($file = $request->file('image')){
            
            $name = time() . $file->getClientOriginalName();

            $file->move('images', $name);

            
            if($user->photo){
                unlink(public_path() . $user->photo->image);

                $photo = Photo::where('user_id', $id)->first();

                $photo->update(['image'=>$name]);
               
            } else {
                $photo = Photo::create(['image'=>$name]);

                $user->photo()->save($photo);
            }
        }

        return redirect('/admin/users')->with('success', 'User edited successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        // if($user->photo->image){
            // unlink(public_path() . $user->photo->image);
        // }
        $user->delete();

        return redirect('/admin/users')->with('success', "User deleted ");
    }
}
