<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'=>'required|min:4',
            'name'=>'required|string',
            'email'=>'required|email|unique:users',
            'role_id'=>'required',
            'department_id'=>'required',
            'image'=> 'required|mimes:jpeg,jpg,png'
        ];
    }

}
