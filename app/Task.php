<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['title', 'status_id'];

    public function departments()
    {
        return $this->belongsToMany('App\Department');
    }

    public function status(){
        return $this->belongsTo('App\Status');
    }

    
    // public function tasks(){
    //     return $this->hasManyThrough('App\Department', 'App\Task');
    // }
}
