<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = ['name'];

    public function tasks()
    {
        return $this->belongsToMany('App\Task');
    }

    public function AllTasks(){
        return $this->belongsToMany('App\Task');
    }
}
