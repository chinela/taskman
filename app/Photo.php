<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    private $imagePath = '/images/';
    protected $fillable = ['user_id', 'image'];

    public function getImageAttribute($value){
        return $this->imagePath . $value;
    }
}
