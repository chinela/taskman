{{-- @if (count($errors))
    @foreach ($errors->all() as $error)
        {{ $error }}
    @endforeach
@endif --}}

@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif

@if (session('failed'))
    <div class="alert alert-danger">
        {{ session('failed') }}
    </div>
@endif