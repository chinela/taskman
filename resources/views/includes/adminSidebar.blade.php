<div class="col-md-2 bg-light py-2">
    <h6><strong>USER SECTION</strong></h6>
    <ul class="nav flex-column">
        <li class="nav-item"><a href="{{ route('admin') }}" class=" nav-link {{ Request::is('admin') ? 'active' : ''}}">Add New User</a></li>
        <li class="nav-item"><a href="{{ route('users.index') }}" class="nav-link {{ Request::is('admin/users') ? 'active' : ''}}">View All Users</a></li>
    </ul>

    <hr class="bg-light">
    
    <h6><strong>TASK SECTION</strong></h6>
    <ul class="nav flex-column">
        <li class="nav-item"><a href="{{ route('tasks.create') }}"class="nav-link {{ Request::is('admin/tasks/create') ? 'active' : ''}}">Add New Task</a></li>
        <li class="nav-item"><a href="{{ route('tasks.index') }}"class="nav-link {{ Request::is('admin/tasks') ? 'active' : ''}}">View All Tasks</a></li>
    </ul>

    <hr class="bg-light">
    
    <h6><strong>DEPARTMENT SECTION</strong></h6>
    <ul class="nav flex-column">
        <li class="nav-item"><a href="{{ route('department.create') }}"class="nav-link {{ Request::is('admin/department/create') ? 'active' : ''}}">Add New Department</a></li>
        <li class="nav-item"><a href="{{ route('department.index' ) }}"class="nav-link {{ Request::is('admin/department') ? 'active' : ''}}">View All Departments</a></li>
    </ul>
</div>

