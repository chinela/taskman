@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h3>Hello {{ $user->name }}</h3>
                    <ul>
                        <li>Email: {{ $user->email }}</li>
                        <li>Level: {{ $user->role->name }}</li>
                        <li>Department: {{ $user->department->name }}</li>
                    </ul>
                </div>
                
            </div>
            @if (!Auth::user()->isAdmin('Admin'))
         
                @if ($tasks->count() > 0)
                
                    <table class="table table-stripped">
                        <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Task</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($tasks as $task)
                                <tr>
                                    <td>{{ $x++ }}</td>
                                    <td>{{ $task->title }}</td>
                                    <td class="{{ $task->status->name == 'Completed' ? 'text-success' : 'text-primary' }}">{{ $task->status->name }}</td>
                                    @if ($task->status->name != 'Completed')
                                    <td>
                                        {{ Form::open(['method'=>'PUT','action'=>['TasksController@completed', $task->id]]) }}
                                            {{ Form::submit('&#8730;', ['class'=>'btn btn-success'])}}
                                        {{ Form::close() }}
                                    </td>
                                    <td>
                                        {{ Form::open() }}
                                            {{ Form::button('X', ['class'=>'btn btn-danger'])}}
                                        {{ Form::close() }}
                                    </td> 
                                    @endif 
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                @endif
            @endif
        </div>
    </div>
</div>
@endsection
