@extends('layouts.app')

@section('content')
    @include('includes.adminBanner')

    <div class="container">
        <div class="row">
            @include('includes.adminSidebar')
            <div class="col">
                <div class="col">
                    @include('includes.message')
                    <h2>Add New Department</h2>
                    {{ Form::open(['action'=>'AdminDeptController@store']) }}

                    <div class="form-group">
                        {{ Form::label('name', 'Department Name:') }}
                        {{ Form::text('name', null, ['class'=> $errors->has('name') ? 'form-control is-invalid' : 'form-control'])}}
                        @foreach ($errors->get('name') as $name)
                            <span class="invalid-feedback mb-3">{{ $name }}</span>
                        @endforeach

                    </div>
                    <div class="form-group">
                        {{ Form::submit('Create', ['class'=>'btn btn-dark'])}}
                    </div>

                    {{ Form::close() }}
                </div>
            </div>
        </div>

    </div>
@endsection