@extends('layouts.app')

@section('content')
    @include('includes.adminBanner')

    <div class="container">
        <div class="row">
            @include('includes.adminSidebar')
            <div class="col">
                <div class="col">
                    @include('includes.message')
                    <h2>View All Departments</h2>
                    @if (count($departments))
                        <table class="table">
                            <thead>
                                <tr class=" bg-info">
                                    <th>S/N</th>
                                    <th>Name</th>
                                    <th>Created_at</th>
                                    <th>Updated_at</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($departments as $department)
                                    <tr>
                                        <td>{{ $x++ }}</td>
                                        <td>{{ $department->name }}</td>
                                        <td>{{ $department->created_at->diffForHumans() }}</td>
                                        <td>{{ $department->updated_at->diffForHumans() }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <h2>No Department</h2>
                    @endif
                </div>
            </div>
        </div>

    </div>
@endsection