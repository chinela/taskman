@extends('layouts.app')

@section('content')
    @include('includes.adminBanner')

    <div class="container">
        <div class="row">
            @include('includes.adminSidebar')
            <div class="col">
                <div class="col">
                    @include('includes.message')
                    <h2>View All Tasks</h2>
                    @if (count($tasks))
                        <table class="table">
                            <thead>
                                <tr class=" bg-info">
                                    <th>S/N</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Created_at</th>
                                    <th>Updated_at</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($tasks as $task)
                                    <tr>
                                        <td>{{ $x++ }}</td>
                                        <td>{{ $task->title }}</td>
                                        <td>{{ $task->status->name }}</td>
                                        <td>{{ $task->created_at->diffForHumans() }}</td>
                                        <td>{{ $task->updated_at->diffForHumans() }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $tasks->render() !!}
                    @else
                        <h2>No Task Available</h2>
                    @endif
                </div>
            </div>
        </div>

    </div>
@endsection