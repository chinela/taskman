@extends('layouts.app')

@section('content');
    
@include('includes.adminBanner')

    <div class="container">
        <div class="row">
            @include('includes.adminSidebar')
            <div class="col">
                <div class="col">
                    @include('includes.message')
                    <h2>Create Task</h2>
                    {!!  Form::open(['action'=>'AdminTasksController@store']) !!}
                        <div class="form-group">
                            {{ Form::label('title', 'Title:') }}
                            {{ Form::text('title', null, ['class'=> $errors->has('title') ? 'form-control is-invalid' : 'form-control'])}}
                            @foreach ($errors->get('title') as $title)
                                <span class="invalid-feedback mb-3">{{ $title }}</span>
                            @endforeach
                        </div>
                        <div class="form-group">
                            {{ Form::label('department', 'Select Department:') }}
                            {!! Form::select('department_id', [''=>'Select Department']+ $departments , '', ['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {{ Form::submit('Add Task', ['class'=>'btn btn-primary'])}}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
@endsection