@extends('layouts.app')

@section('content');
    
@include('includes.adminBanner')

    <div class="container">
        <div class="row">
            @include('includes.adminSidebar')
            <div class="col-2">
                <img src="{{ $user->photo ? $user->photo->image : ' ' }}" alt="" class="img-fluid">
            </div>
            <div class="col">
                <div class="col">
                    @include('includes.message')
                    
                    <h2>Edit User</h2>
                    @if (count($errors))
                    @foreach ($errors->all() as $error)
                        {{ $error }}
                    @endforeach
                @endif
                    {!!  Form::model($user, ['method'=>'PATCH', 'action'=>['AdminUsersController@update', $user->id], 'files'=>true]) !!}
                        <div class="form-group">
                            {{ Form::label('name', 'Name:') }}
                            {{ Form::text('name', null, ['class'=> $errors->has('name') ? 'form-control is-invalid' : 'form-control'])}}
                            @foreach ($errors->get('name') as $name)
                                <span class="invalid-feedback mb-3">{{ $name }}</span>
                            @endforeach
                        </div>
                        <div class="form-group">
                            {{ Form::label('password', 'Password:') }}
                            {{ Form::password('password', ['class'=>'form-control'])}}
                        </div>
                        {{-- <div class="form-group">
                            {{ Form::label('confirm', 'Confirm Password:') }}
                            {{ Form::password('confirm', ['class'=>'form-control'])}}
                        </div> --}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('role', 'Select Role:') }}
                                    {{ Form::select('role_id', $roles, null,  ['class'=>'form-control'])}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('department', 'Select Department:') }}
                                    {!! Form::select('department_id', $departments , null, ['class'=>'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::file('image', ['class'=>' form-control'])}}
                        </div>
                        <div class="form-group">
                            {{ Form::submit('Edit User', ['class'=>'btn btn-primary'])}}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
@endsection