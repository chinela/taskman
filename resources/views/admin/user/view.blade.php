@extends('layouts.app')

@section('content')
    @include('includes.adminBanner')

    <div class="container">
        <div class="row">
            @include('includes.adminSidebar')
            <div class="col">
                <div class="col">
                    @include('includes.message')
                    <h2>View All Users</h2>
                    @if (count($users))
                        <table class="table">
                            <thead>
                                <tr class=" bg-info">
                                    <th>S/N</th>
                                    <th>Images</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Department</th>
                                    <th>Created_at</th>
                                    {{-- <th>Updated_at</th> --}}
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $i=>$user)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td><img src="{{ $user->photo ? $user->photo->image : 'No' }}" alt="" height="30"></td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->role->name }}</td>
                                        {{-- <td>{{ $user->department->name }}</td> --}}
                                        @if (is_null($user->department_id))
                                            <td>No Dept</td>
                                        @else
                                            <td>{{ $user->department->name }}</td>
                                        @endif
                                        <td>{{ $user->created_at->diffForHumans() }}</td>
                                        {{-- <td>{{ $user->updated_at->diffForHumans() }}</td> --}}
                                        <td>
                                            <a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary">Edit</a>
                                        </td>
                                        <td>
                                            {{ Form::open(['method' => 'DELETE', 'action' => ['AdminUsersController@destroy', $user->id]]) }}
                                                {{ Form::submit('Delete', ['class'=>'btn btn-danger'])}}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <h2>No user</h2>
                    @endif
                </div>
            </div>
        </div>

    </div>
@endsection