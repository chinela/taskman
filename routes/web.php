<?php

use Illuminate\Support\Facades\Auth;
use App\User;
use App\Role;
use App\Department;
use App\Task;
use App\TaskStatus;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('admin/users', 'AdminUsersController')->except([
    'show', 'create'
]);
Route::resource('admin/tasks', 'AdminTasksController')->except([
    'show'
]);
Route::get('admin', [
    'as' => 'admin',
    'uses' => 'AdminController@index'
]);
Route::resource('/admin/department', 'AdminDeptController')->except([
    'show'
]);

Route::put('task/complete/{id}', [
    'as' => 'task.complete',
    'uses' => 'TasksController@completed'
]);


